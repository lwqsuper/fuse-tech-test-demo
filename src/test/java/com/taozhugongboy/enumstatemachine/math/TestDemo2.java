package com.taozhugongboy.enumstatemachine.math;

import com.taozhugongboy.enumstatemachine.math.t.TMain;
import org.junit.Test;

/**
 * @Author：Liuwq-Fuse
 * @Package：com.taozhugongboy.enumstatemachine.math
 * @Project：fuse-tech-test-demo
 * @name：TestDemo2
 * @Date：2023/8/25 17:42
 * @Filename：TestDemo2
 */
public class TestDemo2 {

    @Test
    public void main() {
        TMain t = new TMain();
        double x =1;
        System.out.println( t.input(x));
    }
}
