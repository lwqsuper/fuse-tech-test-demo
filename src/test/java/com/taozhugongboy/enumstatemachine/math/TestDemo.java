package com.taozhugongboy.enumstatemachine.math;

import com.taozhugongboy.enumstatemachine.SpringApplication;
import com.taozhugongboy.enumstatemachine.math.i.ActiveFunction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @Author：Liuwq-Fuse
 * @Package：com.taozhugongboy.enumstatemachine.math
 * @Project：fuse-tech-test-demo
 * @name：TestDemo
 * @Date：2023/8/25 16:53
 * @Filename：TestDemo
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringApplication.class)
public class TestDemo {
    @Autowired
    ActiveFunction activeFunction;

    @Test
    public void main() {
        System.out.println( activeFunction.function(1));
    }
}
