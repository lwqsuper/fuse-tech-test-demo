package com.taozhugongboy.enumstatemachine.async;

import com.alibaba.cola.statemachine.StateMachine;
import com.taozhugongboy.enumstatemachine.SpringApplication;
import com.taozhugongboy.enumstatemachine.event.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @Author：Liuwq-Fuse
 * @Package：com.taozhugongboy.enumstatemachine.async
 * @Project：enumstatemachine-master
 * @name：AsyncTest
 * @Date：2023/7/19 9:52
 * @Filename：AsyncTest
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringApplication.class)
public class AsyncTest {
//    @Resource(name = "asyncServiceImpl")
//    private AsyncServiceImpl asyncServiceImpl;
    @Resource
    AsyncService asyncService;
    @Autowired
    private UserService userService;

    @Test
    public void test() {
        //String result = asyncService.sendEmail("e","s","c");
        //System.out.println(result);

        userService.register("坤坤");
    }
}
