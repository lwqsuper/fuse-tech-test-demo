package com.taozhugongboy.enumstatemachine.function_strategy.consumer;
/**
 * 业务异常
 * @author Luzhengning
 * @date 2023年03月02日 10:19
 */
public class ServiceException extends Exception{

    private Object data;
    private int code=500;

    public ServiceException(){ }
    public ServiceException(String message){
        super(message);
    }
    public ServiceException(String message, int code){
        super(message);
        this.code=code;
    }
    public ServiceException(Object data, String message, int code){
        super(message);
        this.data=data;
        this.code=code;
    }
    public Object getData(){
        return this.data;
    }
    public int getCode(){return this.code;}

    public void printStackTrace(){
        super.printStackTrace();
    }
}
