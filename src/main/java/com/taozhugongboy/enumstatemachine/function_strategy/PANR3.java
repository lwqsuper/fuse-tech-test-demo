package com.taozhugongboy.enumstatemachine.function_strategy;

import com.taozhugongboy.enumstatemachine.function_strategy.consumer.ServiceException;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * 有参数有返回值的策略模式
 *
 * @Author：Liuwq-Fuse
 * @Package：com.taozhugongboy.enumstatemachine.function_strategy
 * @Project：enumstatemachine-master
 * @name：PANR3
 * @Date：2023/7/21 16:05
 * @Filename：PANR3
 */
public class PANR3 {
    static Map<String, Function<String,Object>> map = new HashMap();
    static {
        map.put(RoleContant.BA,s -> BA(s));
        /*map.put(RoleContant.TEACH,s -> {
            try {
                return JJ(s);
            } catch (ServiceException e) {
                e.printStackTrace();
            }
            return null;
        });*/
    }

    public static Object BA(String name){
        String str = "BA谁都包补了"+name;
        System.out.println(str);
        return str;
    }

    public static Object JJ(String name) throws ServiceException {
        if(name.equals("jj")){
            throw new ServiceException("审核记录保存失败");
        }else {
            String str = "JJ都包补了"+name;
            return str;
        }
    }
    public static Object handle(String testConstant,String value){
       return map.get(testConstant).apply(value);
    }

    public static void main(String[] args) {
        //handle(TestConstant.Invalid,"王麻子");
        Object key = handle("BA","王麻子");
        System.out.println(key.toString());
        //handlepp("BA","王麻子");
    }
}
