package com.taozhugongboy.enumstatemachine.test.file;

import org.springframework.util.StreamUtils;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;

/**
 * @Author：Liuwq-Fuse
 * @Package：com.taozhugongboy.enumstatemachine.test.file
 * @Project：fuse-tech-test-demo
 * @name：FileToInputStreamExample
 * @Date：2023/9/1 15:56
 * @Filename：FileToInputStreamExample
 */
public class FileToInputStreamExample {
    public static InputStream urlToInputStream(String url) throws IOException {
        // 连接超时(豪秒)
        int connectTimeout = 10000;
        // 接收数据超时(豪秒)
        int receiveTimeout = 300000;
        URLConnection connection = new URL(url).openConnection();
        connection.setRequestProperty("User-Agent", "Mozilla/5.0");
        connection.setConnectTimeout(connectTimeout);
        connection.setReadTimeout(receiveTimeout);
        connection.connect();
        String contentType = connection.getContentType();
        System.out.println("ContentType: " + contentType);
        System.out.println("length: " + connection.getContentLength());
        return connection.getInputStream();
    }

    public static void pfile(String url) throws IOException {
        // 连接超时(豪秒)
        int connectTimeout = 10000;
        // 接收数据超时(豪秒)
        int receiveTimeout = 300000;
        File file = new File(url);
        //System.out.println(": " + file.);
        InputStream stream = new FileInputStream(file);
        byte[] bytes = new byte[(int) file.length()];
        System.out.println("bytes: " + bytes);
//        URLConnection connection = new URL(url).openConnection();
//        connection.setRequestProperty("User-Agent", "Mozilla/5.0");
//        connection.setConnectTimeout(connectTimeout);
//        connection.setReadTimeout(receiveTimeout);
//        connection.connect();
//        String contentType = connection.getContentType();
//        System.out.println("ContentType: " + contentType);
//        return connection.getInputStream();
    }
//    public static void pfile2(String url) throws IOException {
//
//        byte[] bytes = StreamUtils.copyToByteArray(is);
//        is.read(bytes);
//    }


    public static void main1(String[] args) throws IOException {
        pfile("http://fp.baiwang.com/format/d?d=0B66DE6C5688CA36F6FC96196DBFF3F78426EBB15A9F00DDFE941D1B9254CB0DE18A801590D577385EF2162A3EA5AC2C");

    }


//    public static void downloadFile(HttpServletResponse response, InputStream inputStream, String fileName, String contentType) {
//        try (BufferedOutputStream out = new BufferedOutputStream(response.getOutputStream())) {
//            //通知浏览器以附件形式下载
//            response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", fileName));
//            //文件输出格式
//            response.setContentType(contentType);
//            byte[] input = new byte[1024];
//            int len;
//            while ((len = inputStream.read(input)) != -1) {
//                out.write(car, 0, len);
//            }
//        } catch (IOException e) {
//            log.error("Method:downloadFile,ErrorMsg:{}", e.getMessage());
//        }
//    }

    public static void main(String[] args) {
        try {
            //InputStream inputStream = urlToInputStream("https://example.com/image.jpg");
            InputStream inputStream = urlToInputStream("http://fp.baiwang.com/format/d?d=0B66DE6C5688CA36F6FC96196DBFF3F78426EBB15A9F00DDFE941D1B9254CB0DE18A801590D577385EF2162A3EA5AC2C");
//            byte[] bytes = StreamUtils.copyToByteArray(inputStream);
//            inputStream.read(bytes);
//            System.out.println("size: " +  bytes.length);
//            // do something with inputStream
//            System.out.println("size: " +  inputStream.available());
//            System.out.println("size: " +  inputStream.available());
//            System.out.println("size: " +  inputStream.available());
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
