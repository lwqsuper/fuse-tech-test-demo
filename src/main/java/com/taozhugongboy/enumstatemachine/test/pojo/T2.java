package com.taozhugongboy.enumstatemachine.test.pojo;

import lombok.Data;

/**
 * @Author：Liuwq-Fuse
 * @Package：com.taozhugongboy.enumstatemachine.test.pojo
 * @Project：fuse-tech-test-demo
 * @name：T2
 * @Date：2023/8/17 14:41
 * @Filename：T2
 */
@Data
public class T2 {
    private String id;

    private String name;
}
