package com.taozhugongboy.enumstatemachine.test.pojo;

import java.math.BigDecimal;

/**
 * @Author：Liuwq-Fuse
 * @name：FormShow
 * @Date：2023-12-04 11:13
 */
public class FormShow {
    private String name;



    /**
     * 总金额 本次
     */
    private BigDecimal totalAmount;

    public FormShow(String name, BigDecimal totalAmount) {
        this.name = name;
        this.totalAmount = totalAmount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }
}
