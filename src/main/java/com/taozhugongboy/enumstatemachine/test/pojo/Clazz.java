package com.taozhugongboy.enumstatemachine.test.pojo;

/**
 * @Author：Liuwq-Fuse
 * @Package：com.taozhugongboy.enumstatemachine.test.pojo
 * @Project：fuse-tech-test-demo
 * @name：XXClazz
 * @Date：2023/8/3 16:23
 * @Filename：XXClazz
 */
public class Clazz {
    //班级号
    private String clazzNo;
    //学生学号
    private String studentNumber;

    public String getClazzNo() {
        return clazzNo;
    }

    public void setClazzNo(String clazzNo) {
        this.clazzNo = clazzNo;
    }

    public String getStudentNumber() {
        return studentNumber;
    }

    public void setStudentNumber(String studentNumber) {
        this.studentNumber = studentNumber;
    }

    public Clazz(String clazzNo, String studentNumber) {

        this.clazzNo = clazzNo;
        this.studentNumber = studentNumber;
    }
}
