package com.taozhugongboy.enumstatemachine.test.pojo;

/**
 * @Author：Liuwq-Fuse
 * @Package：com.taozhugongboy.enumstatemachine.test.pojo
 * @Project：fuse-tech-test-demo
 * @name：Student
 * @Date：2023/8/3 16:22
 * @Filename：Student
 */
/**
 * 学生
 */
public class Student {
    //学号
    private String number;
    //班级
    private String clazzNo;
    //姓名
    private String name;
    //年龄
    private int age;

    private boolean cgVerifyResult;

    public boolean isCgVerifyResult() {
        return cgVerifyResult;
    }

    public void setCgVerifyResult(boolean cgVerifyResult) {
        this.cgVerifyResult = cgVerifyResult;
    }

    public Student(String number, String name, int age) {
        this.number = number;
        this.name = name;
        this.age = age;
    }

    public Student(String number, String name, int age,boolean cgVerifyResult) {
        this.number = number;
        this.name = name;
        this.age = age;
        this.cgVerifyResult = cgVerifyResult;
    }


    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getClazzNo() {
        return clazzNo;
    }

    public void setClazzNo(String clazzNo) {
        this.clazzNo = clazzNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "number='" + number + '\'' +
                ", clazzNo='" + clazzNo + '\'' +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", cgVerifyResult=" + cgVerifyResult +
                '}';
    }
}
