package com.taozhugongboy.enumstatemachine.test.pojo;

/**
 * @Author：Liuwq-Fuse
 * @Package：com.taozhugongboy.enumstatemachine.test.pojo
 * @Project：fuse-tech-test-demo
 * @name：T1
 * @Date：2023/8/17 14:41
 * @Filename：T1
 */
public class T1 {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
