package com.taozhugongboy.enumstatemachine.test;

import com.alibaba.fastjson2.JSON;
import com.taozhugongboy.enumstatemachine.test.pojo.Clazz;
import com.taozhugongboy.enumstatemachine.test.pojo.Student;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Author：Liuwq-Fuse
 * @Package：com.taozhugongboy.enumstatemachine.test
 * @Project：fuse-tech-test-demo
 * @name：Test
 * @Date：2023/8/3 16:23
 * @Filename：Test
 */
public class Test {
    /**
     *过滤出两个LIST中想要比较属性相同的对象
     * @param args
     */
    public static void main(String[] args) {
        List<Student> students=new ArrayList<Student>();
        Student s1=new Student("101","李星云",20);
        Student s2=new Student("102","陆林轩",18);
        Student s3=new Student("103","常宣灵",25);
        Student s4=new Student("104","坤坤",27);
//        Student s4=new Student("202008310202","常昊灵",25);
//        Student s5=new Student("202008310301","杨焱",25);
//        Student s6=new Student("202008310302","杨淼",25);
        students.add(s1);
        students.add(s2);
        students.add(s3);
        students.add(s4);
//        students.add(s4);
//        students.add(s5);
//        students.add(s6);
        List<Clazz> clazzes=new ArrayList<Clazz>();
        Clazz c1=new Clazz("01","101");
        Clazz c2=new Clazz("01","102");
        //Clazz c3=new Clazz("02","202008310201");
//        Clazz c4=new Clazz("02","202008310202");
//        Clazz c5=new Clazz("03","202008310301");
//        Clazz c6=new Clazz("03","202008310302");
        clazzes.add(c1);
        clazzes.add(c2);
        //clazzes.add(c3);
//        clazzes.add(c4);
//        clazzes.add(c5);
//        clazzes.add(c6);
        //过滤出两个不同对象LIST中想要比较属性相同的 此方法需注意循环对象是哪个！ 而且不能比较不相等的！
        List<Student> result = new ArrayList<>();
        for (Clazz clazz:clazzes){
            //students.stream().filter(s ->s.getNumber().equals(clazz.getStudentNumber())).forEach(s -> s.setClazzNo(clazz.getClazzNo()));
            List<Student> filter = students.stream().filter(s ->s.getNumber().equals(clazz.getStudentNumber())).collect(Collectors.toList());
            if(!CollectionUtils.isEmpty(filter)) {
                //Integer invoiceQuantity = result.get(0).getInvoiceQuantity();//已开票数量
                result.add(filter.get(0));
            }
            //students.stream().filter(s ->s.getNumber().equals(clazz.getStudentNumber()));
            //System.out.println(students.get(0).toString());
            //System.out.println(JSON.toJSONString(t2));
        }

        for (Clazz clazz:clazzes){
            for (Student student: students) {
                if(student.getNumber().equals(clazz.getStudentNumber())){

                }
            }
        }

        //先分组
        Map<String, List<Student>> map =
                students.stream().collect(Collectors.groupingBy(Student::getNumber));
        for (Clazz clazz:clazzes){
            if(ObjectUtils.isNotEmpty(map.get(clazz.getStudentNumber()))){
                map.remove(clazz.getStudentNumber());//remove掉重复的
            }
        }
        for (Map.Entry<String, List<Student>> entry : map.entrySet()) {
            System.out.println("key= " + entry.getKey() + " and value= " + entry.getValue().get(0));//遍历不重复的
        }


        //List<Student> reduce1 = students.stream().filter(s ->!s.getNumber().equals(clazzes.getStudentNumber())).collect(Collectors.toList());
//        List<ErpPurchaseInvoiceFormPriceDTO> result = purchaseInvoiceFormPrices.stream()
//                .filter(s ->s.getPurchaseAdjustPrice().getId().equals(adjustPriceDTO.getId())).collect(Collectors.toList());
//        if(CollectionUtil.isNotEmpty(result)){
//            Integer invoiceQuantity = result.get(0).getInvoiceQuantity();//已开票数量
//        System.out.println(JSON.toJSONString(result));
//        result.forEach(System.out::println);


//        List<Student> reduce1 = students.stream().filter(item -> !result.contains(item)).collect(Collectors.toList());
//        System.out.println(JSON.toJSONString(reduce1));
    }
//根据 List<Object> 中 Object 某个属性去重
    //        List<ErpCommodityDTO> result = list.stream().collect(
//                collectingAndThen(
//                        toCollection(() -> new TreeSet<>(Comparator.comparing(ErpCommodityDTO::getId))), ArrayList::new)
//        );
//        list.clear();
//        list.addAll(result);

    /**
     * 在内存中 list排序
     * @param args
     * @author Liuwq
     * @date 2023/09/18
     */
    public static void mainSort(String[] args) {
        List<Student> students = new ArrayList<Student>();
        Student s1 = new Student("101", "李星云", 20,true);
        Student s2 = new Student("102", "陆林轩", 18,false);
        Student s3 = new Student("103", "常宣灵", 25,true);
        students.add(s1);
        students.add(s2);
        students.add(s3);

        students =students.stream().sorted(Comparator.comparing(Student::isCgVerifyResult).reversed()).collect(Collectors.toList());//item->item.isCgVerifyResult()
        students.forEach(System.out::println);
    }

    public static void main12(String[] args) {
        List<Student> students = new ArrayList<Student>();
        Student s1 = new Student("101", "李星云", 20);
        Student s2 = new Student("102", "陆林轩", 18);
        Student s3 = new Student("103", "常宣灵", 25);

        students.add(s1);
        students.add(s2);
        students.add(s3);

        List<Clazz> clazzes = new ArrayList<Clazz>();
        Clazz c1 = new Clazz("01", "101");
        Clazz c2 = new Clazz("01", "102");

        clazzes.add(c1);
        clazzes.add(c2);
        /*List<Student> list = students.stream()
                .map(student -> clazzes.stream()
                        .filter(clazz -> student.getNumber().equals(clazz.getStudentNumber()))
                        .findFirst()
                        .map(clazz -> {
                            student.setClazzNo(clazz.getClazzNo());
                            return student;
                        }).orElse(null))
                .collect(Collectors.toList());*/
        //过滤出两个不同对象LIST中想要比较属性相同的
        List<Student> list = students.stream()
                .map(student -> clazzes.stream()
                        .filter(clazz -> student.getNumber().equals(clazz.getStudentNumber()))
                        .findFirst()
                        .map(clazz -> {
                            student.setClazzNo(clazz.getClazzNo());
                            return student;
                        }).orElse(null))
                .collect(Collectors.toList());
        System.out.println(list.toString());
    }


}
