package com.taozhugongboy.enumstatemachine.test.utils;

import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @Author：Liuwq-Fuse
 * @Package：com.taozhugongboy.enumstatemachine.test.utils
 * @Project：fuse-tech-test-demo
 * @name：CollectionExtUtils
 * @Date：2023/8/18 9:56
 * @Filename：CollectionExtUtils
 */
public class CollectionExtUtils extends CollectionUtils {
    public static <T> List<T> convertTarget(List sourceList, Class<T> targetClass) {
        ArrayList targetList = new ArrayList();
        try {
            Iterator values = sourceList.iterator();
            while (values.hasNext()) {
                Object source = values.next();
                T target = targetClass.newInstance();
                BeanUtils.copyProperties(source, target);
                targetList.add(target);
            }
        } catch (Exception e) {
            System.out.println(">>>>>>复制异常<<<<<<" + e);
        }
        return targetList;
    }
}
