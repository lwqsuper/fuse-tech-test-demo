package com.taozhugongboy.enumstatemachine.test;

import java.util.Random;

/**
 * @Author：Liuwq-Fuse
 * @name：YanZhenMa
 * @Date：2023-12-07 9:58
 */
public class YanZhenMa {
    public static void main(String[] args) {
//        调用verificationCode方法，并给定验证码长度
//        String verification_code = verificationCode(6);
        String verification_code = verificationCodeNum(6);
        System.out.println("验证码为:" + verification_code);
    }
    //定义verificationCode方法
    public static String verificationCode(int a) {
//        定义一个空字符串
        String verification_code = "";
//        创建Random对象
        Random r = new Random();
        for (int i = 0; i < a; i++) {
//            确定字符类型(0代表数字，1代表大写字母，2代表小写字母)
            int type = r.nextInt(3);
            switch (type) {
                case 0:
//                    随机生成一个0~9数字
                    verification_code += r.nextInt(10);
                    break;
                case 1:
//                    随机生成一个65~90的数字，然后强转为大写字母
                    verification_code += (char) (r.nextInt(26) + 65);
                    break;
                case 2:
//                    随机生成一个97~122的数字，然后强转为小写字母
                    verification_code += (char) (r.nextInt(26) + 97);
                    break;
            }
        }
        return verification_code;
    }

    public static String verificationCodeNum(int a) {
//        定义一个空字符串
        String verification_code = "";
//        创建Random对象
        Random r = new Random();
        for (int i = 0; i < a; i++) {
            verification_code += r.nextInt(10);
//            确定字符类型(0代表数字，1代表大写字母，2代表小写字母)
        }
        return verification_code;
    }
}
