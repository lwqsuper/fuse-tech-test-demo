package com.taozhugongboy.enumstatemachine.test;

import java.util.Arrays;

/**
 * 商品类型枚举
 *
 * @author Liuwq
 * @Author：Liuwq-Fuse
 * @Package：com.zxhy.service.erp.product.constant
 * @Project：zhixing-heyue-erp-server-dev
 * @name：CommodityType
 * @Date：2023/7/6 10:24
 * @Filename：CommodityType
 * @date 2023/07/06
 */
public enum CommodityTypeEnum {
    /**
     * 药物
     */
    Drugs("1","药品"),

    /**
     * 装置
     */
    Apparatus("2", "器械"),

    /**
     * 消毒
     */
    Sterilized("3", "消杀"),

    /**
     * 食物
     */
    Food("4", "食品"),
    ;
    /**
     * 代码
     */
    private String code;


    /**
     * 消息
     */
    private String message;

    private CommodityTypeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    /**
     * 得到消息
     *
     * @return {@link String }
     * @author Liuwq
     * @date 2023/07/06
     */
    public String getMessage() {
        return message;
    }

    /**
     * 获取代码
     *
     * @return {@link String }
     * @author Liuwq
     * @date 2023/07/06
     */
    public String getCode() {
        return code;
    }





    @Override
    public String toString() {
        return this.code+":"+this.message;
    }

    /**
     * code转成枚举
     *
     * @param code 代码
     * @return {@link CommodityTypeEnum }
     * @author Liuwq
     * @date 2023/07/06
     */
    public static CommodityTypeEnum codeOf(String code) {
        return Arrays.stream(CommodityTypeEnum.class.getEnumConstants()).filter(e -> e.getCode().equals(code)).findAny().orElse(Drugs);
    }

    public static void main(String[] args) {
        CommodityTypeEnum e = CommodityTypeEnum.valueOf("Drugs");
        System.out.println(e.getMessage());

        CommodityTypeEnum e1 = CommodityTypeEnum.codeOf("2");
        System.out.println(e1.getMessage());
    }
}
