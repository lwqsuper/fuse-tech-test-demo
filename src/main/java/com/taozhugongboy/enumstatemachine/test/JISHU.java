package com.taozhugongboy.enumstatemachine.test;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author：Liuwq-Fuse
 * @Package：com.taozhugongboy.enumstatemachine.test
 * @Project：fuse-tech-test-demo
 * @name：JISHU
 * @Date：2023/9/12 15:32
 * @Filename：JISHU
 */
public class JISHU {
    public static void main(String[] args) {

        List<String> namesList = new ArrayList<>();
        namesList.add("gaurav");
        namesList.add("deepak");

        List<String> ppList = new ArrayList<>();
        ppList.add("111");
        ppList.add("222");
        ppList.add("333");

//        for (String s1: namesList) {
//            count = p2(count,ppList);
//        }
        int count = 0;
        for (String s1: namesList) {
            for (String s2: ppList) {
                p2(++count);
            }
        }
//        if (count > 1) {
//            System.out.println("executed");
//        }
    }

    static void p2(int count){
            System.out.println(count);
    }
}
