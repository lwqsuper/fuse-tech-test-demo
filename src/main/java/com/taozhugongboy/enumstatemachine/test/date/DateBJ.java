package com.taozhugongboy.enumstatemachine.test.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

/**
 * @Author：Liuwq-Fuse
 * @name：DateBJ
 * @Date：2023-12-14 13:35
 */
public class DateBJ {
    public static void main22(String[] args) {
        DateTimeFormatter sdf = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        Date date = new Date();
        SimpleDateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        System.out.println(dateFormat.format(date));


        LocalDate date1 = LocalDate.of(2009, 12, 31);

        LocalDate date2 = LocalDate.of(2019, 1, 31);

        System.out.println("date1 : " + sdf.format(date1));

        System.out.println("date2 : " + sdf.format(date2));

//        System.out.println("Is...");

        if (date1.isAfter(date2)) {

            System.out.println("Date1 时间在 Date2 之后");

        }

        if (date1.isBefore(date2)) {

            System.out.println("Date1 时间在 Date2 之前");

        }

        if (date1.isEqual(date2)) {

            System.out.println("Date1 时间与 Date2 相等");

        }
    }

    public static void main(String[] args) throws ParseException {
        Date date = new Date();
//        SimpleDateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//        System.out.println(dateFormat.format(date));

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        //date = sdf.parse(date);

//        Date date1 = sdf.parse("2009-12-31");

        Date date2 = sdf.parse("2023-12-15 00:00:00");

        Calendar cal1 = Calendar.getInstance();

        Calendar cal2 = Calendar.getInstance();

        cal1.setTime(date);

        cal2.setTime(date2);
        if (cal1.equals(cal2)) {
            System.out.println("Date1 时间与 Date2http:// 相等");
        }

        if (cal1.after(cal2)) {
//            System.out.println("Date 时间在 Date2 之后");
            System.out.println("有效期小于当前日期");
        }

//        System.out.println("date1 : " + sdf.format(date1));
//
//        System.out.println("date2 : " + sdf.format(date2));
//
//        Calendar cal1 = Calendar.getInstance();
//
//        Calendar cal2 = Calendar.getInstance();
//
//        cal1.setTime(date1);
//
//        cal2.setTime(date2);
//
//        if (cal1.after(cal2)) {
//
//            System.out.println("Date1 时间在 Date2 之后");
//
//        }
//
//        if (cal1.before(cal2)) {
//
//            System.out.println("Date1 时间在 Date2 之前");
//
//        }
//
//        if (cal1.equals(cal2)) {
//
//            System.out.println("Date1 时间与 Date2http:// 相等");
//
//        }
    }
}
