package com.taozhugongboy.enumstatemachine.test;

import com.alibaba.fastjson2.JSON;
import com.taozhugongboy.enumstatemachine.test.pojo.T1;
import com.taozhugongboy.enumstatemachine.test.pojo.T2;
import com.taozhugongboy.enumstatemachine.test.utils.CollectionExtUtils;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @Author：Liuwq-Fuse
 * @Package：com.taozhugongboy.enumstatemachine.test
 * @Project：fuse-tech-test-demo
 * @name：BeanCopyDemo
 * @Date：2023/8/17 14:39
 * @Filename：BeanCopyDemo
 */
public class BeanCopyDemo {
    public static void main(String[] args) {
        T1 t1 =new T1();
        t1.setName("jj");
        T2 t2 =new T2();
        //t2.setId("001");
        BeanUtils.copyProperties(t1, t2);
        System.out.println(JSON.toJSONString(t2));
        System.out.println(JSON.toJSONString(t1));

//        List<T1> t1List = new ArrayList<>();
//        T1 t1 =new T1();
//        t1.setName("jj");
//        T1 t12 =new T1();
//        t12.setName("pp");
//        t1List.add(t1);
//        t1List.add(t12);
//
//
//        List<T2> t2List = CollectionExtUtils.convertTarget(t1List,T2.class);
//        System.out.println(JSON.toJSONString(t2List));

        //List<T2> t2List = new ArrayList<>();
        //BeanUtils.copyProperties(t1List, t2List);
    }
}
