package com.taozhugongboy.enumstatemachine.test.str;

import com.taozhugongboy.enumstatemachine.test.str.util.StringUtil;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @Author：Liuwq-Fuse
 * @name：StringUtilsExample
 * @Date：2023-10-19 9:39
 */
public class StringUtilsExample {
    public static void main1(String[] args) {
//        String str = "abc123def456";
//        String str = "varchar(250)";
        String str = "float(5,1)";
        String number = StringUtils.getDigits(str);

        System.out.println(number);
    }

    public static void main23(String[] args) {
        String input = "Hello World! This is a sample string.";

        // 创建正则表达式对象并编译
        Pattern pattern = Pattern.compile("World!");

        // 在输入中查找与模式匹配的第一个子串
        Matcher matcher = pattern.matcher(input);

        if (matcher.find()) {
            int startIndex = matcher.start(); // 获取匹配到的子串起始位置索引

            // 根据起始位置索引进行切片操作
            String result = input.substring(startIndex + 1);

            System.out.println(result);
        } else {
            System.out.println("未找到匹配的子串");
        }
    }

    public static void main(String[] args) {
//        String input = "订单编号：CD202310310016  电汇20231031015[采购订单-李惠敏, 采购收货单-邓鹏飞1, 采购验收单-王玉顺, 上架确认单-邓鹏飞1]   发票号码:23502000000037371601";
//        String input = "CD202310310016";
//        // 创建正则表达式对象并编译
//        Pattern pattern = Pattern.compile("CD");
        String input = "申请单编号：CG-2023-12-01-0002  李东 99023【WMS回传】";
        // 创建正则表达式对象并编译
        Pattern pattern = Pattern.compile("CG");


        // 在输入中查找与模式匹配的第一个子串
        Matcher matcher = pattern.matcher(input);

        if (matcher.find()) {
            int startIndex = matcher.start(); // 获取匹配到的子串起始位置索引

            // 根据起始位置索引进行切片操作
            String result = input.substring(startIndex, startIndex + 18);

            System.out.println(result);
        } else {
            System.out.println("未找到匹配的子串");
        }
    }
    public static void mainjb(String[] args) {
//        String input = "订单编号：CD202310310016  电汇20231031015[采购订单-李惠敏, 采购收货单-邓鹏飞1, 采购验收单-王玉顺, 上架确认单-邓鹏飞1]   发票号码:23502000000037371601";
//        String regx="CD\\d{12}";

        String input = "申请单编号：CG-2023-12-01-0002  李东 99023【WMS回传】";
        String regx="CG-\\d{12}";
        Pattern compile = Pattern.compile(regx);
        Matcher matcher = compile.matcher(input);
        if(matcher.find()){
        String orderNo = matcher.group();
            System.out.println(orderNo);
         }
    }




    public static void main22(String[] args) {

        //String str = "有12只猫和13只狗。";

//        String str = "float(5,1)";
//        Pattern pattern = Pattern.compile("\\d+");
//        Matcher matcher = pattern.matcher(str);
//        List<String> list = new ArrayList<>();
//        while (matcher.find()) {
//            System.out.println(matcher.group());
//            list.add(matcher.group());
//        }
//        System.out.println(list.get(0));
//        System.out.println(list.get(1));

        //String[] arr = new String[]{"a","b","c","dd"};
        String str = "a,b,c,dd";
        //System.out.println(StringUtil.concatStr(arr, ","));

        String[] arr =new String[] {str};

        System.out.println(StringUtil.concatStr(arr, ","));


        //String args = StringUtil.concatStr(realValues.stream().map(e->"?").collect(Collectors.toList()),",");
    }

/*遍历数组*/
}
