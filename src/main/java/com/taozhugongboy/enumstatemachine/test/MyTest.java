package com.taozhugongboy.enumstatemachine.test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author：Liuwq-Fuse
 * @Package：com.taozhugongboy.enumstatemachine.test
 * @Project：fuse-tech-test-demo
 * @name：MyTest
 * @Date：2023/8/15 10:37
 * @Filename：MyTest
 */
public class MyTest {


        public static void main(String[] args) {
            List<String> list1 = new ArrayList<String>();
            list1.add("1");
            list1.add("2");
            list1.add("3");
            list1.add("4");
            list1.add("5");


            System.out.println(list1.contains("1"));

//            List<String> list2 = new ArrayList<String>();
//            list2.add("2");
//            list2.add("3");
//            list2.add("6");
//            list2.add("7");
//            List<String> reduce1 = list1.stream().filter(item -> !list2.contains(item)).collect(Collectors.toList());
//            System.out.println("---差集 reduce1 (list1 - list2)---");
//            reduce1.parallelStream().forEach(System.out :: println);




//            // 交集
//            List<String> intersection = list1.stream().filter(item -> list2.contains(item)).collect(toList());
//            System.out.println("---交集 intersection---");
//            intersection.parallelStream().forEach(System.out :: println);
//
//            // 差集 (list1 - list2)
//            List<String> reduce1 = list1.stream().filter(item -> !list2.contains(item)).collect(toList());
//            System.out.println("---差集 reduce1 (list1 - list2)---");
//            reduce1.parallelStream().forEach(System.out :: println);
//
//            // 差集 (list2 - list1)
//            List<String> reduce2 = list2.stream().filter(item -> !list1.contains(item)).collect(toList());
//            System.out.println("---差集 reduce2 (list2 - list1)---");
//            reduce2.parallelStream().forEach(System.out :: println);
//
//            // 并集
//            List<String> listAll = list1.parallelStream().collect(toList());
//            List<String> listAll2 = list2.parallelStream().collect(toList());
//            listAll.addAll(listAll2);
//            System.out.println("---并集 listAll---");
//            listAll.parallelStream().forEachOrdered(System.out :: println);
//
//            // 去重并集
//            List<String> listAllDistinct = listAll.stream().distinct().collect(toList());
//            System.out.println("---得到去重并集 listAllDistinct---");
//            listAllDistinct.parallelStream().forEachOrdered(System.out :: println);
//
//            System.out.println("---原来的List1---");
//            list1.parallelStream().forEachOrdered(System.out :: println);
//            System.out.println("---原来的List2---");
//            list2.parallelStream().forEachOrdered(System.out :: println);

        }

}
