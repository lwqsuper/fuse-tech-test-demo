package com.taozhugongboy.enumstatemachine.test.lambda;

import com.taozhugongboy.enumstatemachine.test.pojo.FormShow;
import com.taozhugongboy.enumstatemachine.test.pojo.Student;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @Author：Liuwq-Fuse
 * @name：ListJiSuan
 * @Date：2023-12-04 11:00
 */
public class ListJiSuan {
    public static void main(String[] args) {
//        List<Student> students=new ArrayList<Student>();
//        Student s5=new Student("101","坤坤",-21);
//        Student s6=new Student("101","坤坤",-22);
        List<FormShow> fs=new ArrayList<FormShow>();
        FormShow f1=new FormShow("坤坤",new BigDecimal(-10.22));
        FormShow f2=new FormShow("坤坤2",new BigDecimal(-22.34));
        fs.add(f1);
        fs.add(f2);
        List<BigDecimal> ts=fs.stream().map(i->i.getTotalAmount()).collect(Collectors.toList());
        BigDecimal sum = ts.stream()
                .reduce(BigDecimal.ZERO, BigDecimal::add);
                //.orElseThrow(() -> new IllegalStateException("列表为空！"));
        System.out.println(sum);
        BigDecimal  b =sum.setScale(2, RoundingMode.HALF_UP);
        System.out.println(b);
//        List<> students.stream().map(i->i.getAge()).collect(Collectors.toList());
//        int countAge = students.stream().mapToInt(a->a.getAge()).sum();
//        System.out.println(countAge);
    }
}
