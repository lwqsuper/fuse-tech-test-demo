package com.taozhugongboy.enumstatemachine.test.lambda;

import com.alibaba.fastjson2.JSON;
import com.taozhugongboy.enumstatemachine.test.pojo.Clazz;
import com.taozhugongboy.enumstatemachine.test.pojo.Student;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 重复数据演示
 *
 * @author Liuwq
 * @Author：Liuwq-Fuse
 * @Package：com.taozhugongboy.enumstatemachine.test.lambda
 * @Project：fuse-tech-test-demo
 * @name：DuplicateDataDemo
 * @Date：2023/8/23 16:15
 * @Filename：DuplicateDataDemo
 * @date 2023/08/23
 */
public class DuplicateDataDemo {
    /*public static  List<Student> dogs = null;
    static {
        dogs = new ArrayList<Student>(){
            {
                add(new Dog("黄一",11));
                add(new Dog("黄一",22));
                add(new Dog("黄三",33));
            }
        };

    }*/
    //@SuppressWarnings("AlibabaAvoidManuallyCreateThread")
    public static void main(String[] args) {
        List<Student> students = new ArrayList<Student>();
        Student s1 = new Student("101", "李星云", 20);
        Student s2 = new Student("102", "陆林轩", 18);
        Student s3 = new Student("102", "陆林轩", 25);
        Student s4 = new Student("101", "坤坤", 20);
        Student s5 = new Student("101", "坤坤", 21);
        Student s6 = new Student("101", "坤坤", 33);
        students.add(s1);
        students.add(s2);
        students.add(s3);
        students.add(s4);
        students.add(s5);
        students.add(s6);

        List<Student> result = new ArrayList<>();
        result.addAll(students);
        List<Student> students1 = new ArrayList<Student>();
        Student s11 = new Student("104", "历飞鱼", 20);
        Student s21 = new Student("105", "韩利", 18);
        Student s31 = new Student("106", "韩利2", 25);

        students1.add(s11);
        students1.add(s21);
        students1.add(s31);
        result.addAll(students1);

        System.out.println(JSON.toJSONString(result));
    }
    public static void mainDu(String[] args) {
        List<Student> students=new ArrayList<Student>();
        Student s1=new Student("101","李星云",20);
        Student s2=new Student("102","陆林轩",18);
        Student s3=new Student("102","陆林轩",25);
        Student s4=new Student("101","坤坤",20);
        Student s5=new Student("101","坤坤",21);
        Student s6=new Student("101","坤坤",33);
        students.add(s1);
        students.add(s2);
        students.add(s3);
        students.add(s4);
        students.add(s5);
        students.add(s6);
        //添加相同自编码、相同单价系统将该商品本次添加数量与已添加数量进行合并，明细中合并成一条记录
        //合并操作 LIST
        List<Student> result = new ArrayList<>();
        List<Student> calculate = new ArrayList<>();
        for (Student dto:students){

            List<Student> filter = students.stream().filter(s ->
                    s.getNumber().equals(dto.getNumber())&&
                            s.getName().equals(dto.getName())
                    ).collect(Collectors.toList());
            if(!CollectionUtils.isEmpty(filter)&&filter.size()>1) {
                if(!calculate.containsAll(filter)){
                    calculate.addAll(filter);
                }

            }else{
                result.add(dto);
            }
        }
        System.out.println(JSON.toJSONString(calculate));
        Map<String, List<Student>> singleMap = calculate.stream().collect(Collectors.groupingBy(Student::getNumber));
        System.out.println(JSON.toJSONString(singleMap));
        List<String> numbers =  calculate.stream().map(item->item.getNumber()).distinct().collect(Collectors.toList());
        for (String num: numbers) {
            List<Student> stu = singleMap.get(num);
            int countAge = stu.stream().mapToInt(a->a.getAge()).sum();
            stu.get(0).setAge(countAge);
            result.add(stu.get(0));
        }
        System.out.println(JSON.toJSONString(result));


        //singleMap.
//        List<String> list =students.stream().
//                collect(Collectors.groupingBy(s->s.getNumber(),Collectors.counting()))
//                .entrySet().stream()
//                .filter(entry->entry.getValue()>1)
//                .map(entry->entry.getKey())
//                .collect(Collectors.toList());
//
//        System.out.println(list.toString());

//        Test.dogs.forEach(dog-> System.out.println("Lambda1:"+dog.getName()));
//
//        Test.dogs.forEach((dog)-> System.out.println("Lambda2:"+dog.getName()));
//
//        //noinspection AlibabaAvoidManuallyCreateThread
//        new Thread(()->{
//            System.out.println("lambda3:"+Thread.currentThread().getName());
//        }).start();
//
//        StringBuffer sb = new StringBuffer();
//        sb.append("1");
//        Runnable runnable=()-> System.out.println("lambda4:"+Thread.currentThread().getName());
//        new Thread(runnable).start();
//
//        Test.dogs.stream()
//                .filter((dog)->dog.getAge()>18)
//                .forEach((dog)-> System.out.println("lambda5:"+dog.getAge()));
//
//        Consumer<Dog> consumer = (dog)-> System.out.println("lambda6:"+dog.getAge());
//        Predicate<Dog> dogPredicate =(dog)->dog.getAge()>18;
//        Test.dogs.stream()
//                .filter(dogPredicate)
//                .limit(1)
//                .forEach(consumer);

    }
}

