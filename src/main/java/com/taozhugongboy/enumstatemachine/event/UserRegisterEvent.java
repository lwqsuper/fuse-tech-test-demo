package com.taozhugongboy.enumstatemachine.event;

import org.springframework.context.ApplicationEvent;

/**
 * 用户注册事件
 * SpringBoot 七个生命周期回调事件函数，SpringBoot底层是在jdk中的java.util.EventObject类和java.util.EventListener之上封装实现的发布订阅机制。
 * 同时也提供了自定义事件定义，
 * 该种方式是一种设计模式–观察者设计模式，优势在于解耦，模块和模块耦合度降低方便扩展。
 *
 * @author Liuwq
 * @Author：Liuwq-Fuse
 * @Package：com.fusetech.system.event
 * @Project：RuoYi-Vue
 * @name：UserRegisterEvent
 * @Date：2023/5/31 11:24
 * @Filename：UserRegisterEvent
 * @date 2023/05/31
 */
public class UserRegisterEvent extends ApplicationEvent {
    /**
     * 用户名
     */
    private String name;

    public UserRegisterEvent(Object source) {
        super(source);
    }

    /**
     * 用户注册事件
     *
     * @param source 源
     * @param name   名字
     */
    public UserRegisterEvent(Object source, String name) {
        super(source);
        this.name = name;
    }

    public String getUserName() {
        return name;
    }

}
