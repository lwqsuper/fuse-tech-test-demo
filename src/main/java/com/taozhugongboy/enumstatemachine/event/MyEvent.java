package com.taozhugongboy.enumstatemachine.event;

import org.springframework.context.ApplicationEvent;

/**
 * @Author：Liuwq-Fuse
 * @Package：com.fusetech.system.event
 * @Project：RuoYi-Vue
 * @name：MyEvent
 * @Date：2023/5/31 11:17
 * @Filename：MyEvent
 */
public class MyEvent extends ApplicationEvent {

    public MyEvent(Object source) {
        super(source);
    }
}
