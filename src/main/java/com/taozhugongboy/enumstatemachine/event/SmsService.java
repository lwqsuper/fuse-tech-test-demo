package com.taozhugongboy.enumstatemachine.event;

import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * 短信服务
 *
 * @author Liuwq
 * @Author：Liuwq-Fuse
 * @Package：com.fusetech.system.event
 * @Project：RuoYi-Vue
 * @name：SmsService
 * @Date：2023/5/31 11:35
 * @Filename：SmsService
 * @date 2023/06/09
 */
@Service
//@Log
public class SmsService implements ApplicationListener<UserRegisterEvent> {

    /**
     * 应用程序事件
     *
     * @param event 事件
     */
    @Override
    @Async("taskExecutor")
    public void onApplicationEvent(UserRegisterEvent event) {
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //log.info("发送短信给用户：" + event.getUserName());
        System.out.println("发送短信给用户：" + event.getUserName());
    }
}
