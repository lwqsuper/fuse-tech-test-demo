package com.taozhugongboy.enumstatemachine.event;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Service;

/**
 * @Author：Liuwq-Fuse
 * @Package：com.fusetech.system.event
 * @Project：RuoYi-Vue
 * @name：UserService
 * @Date：2023/5/31 11:26
 * @Filename：UserService
 */
@Service
//@Log
public class UserService implements ApplicationEventPublisherAware {
    private ApplicationEventPublisher applicationEventPublisher;

    /**
     * 设置应用程序事件发布者
     *
     * @param applicationEventPublisher 应用程序事件发布者
     */
    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    /*** 注册
     ** @param userName 用户名
     *
     * @author Liuwq
     * @date 2023/06/09
     */
    public void register(String userName) {
        //log.info("用户注册" + userName + ", 数据插入到数据库！");
        System.out.println("用户注册" + userName + ", 数据插入到数据库！");
        // 发布用户注册事件
        applicationEventPublisher.publishEvent(new UserRegisterEvent(this, userName));
    }
}
