package com.taozhugongboy.enumstatemachine.math.function;


import com.taozhugongboy.enumstatemachine.math.i.ActiveFunction;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

/**
 * @param
 * @DATA
 * @Author LiDaPeng
 * @Description
 */
@Service
public class ELu implements ActiveFunction {
    @Override
    public double function(double x) {
        if (x > 0) {
            x = x * 0.1;
        } else {
            x = 0.1 * (Math.exp(x) - 1);
        }
        System.out.println("ELu"+x);
        return x;
    }

    @Override
    public double functionG(double out) {
        if (out > 0) {
            return 0.1;
        } else {
            return out + 0.1;
        }
    }
}
