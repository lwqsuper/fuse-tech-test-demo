package com.taozhugongboy.enumstatemachine.math.function;

import com.taozhugongboy.enumstatemachine.math.i.ActiveFunction;
import org.springframework.stereotype.Service;

/**
 * @author lidapeng
 * @description ReLu 函数
 * @date 8:56 上午 2020/1/11
 */
public class ReLu implements ActiveFunction {
    @Override
    public double function(double x) {
        System.out.println("ReLu"+x * 0.1);
        return x * 0.1;
    }

    @Override
    public double functionG(double out) {
        return 0.1;
    }
}
