package com.taozhugongboy.enumstatemachine.math.testDemo;

/**
 * 汉诺塔
 * 有三个柱子，分别为 from、buffer、to。需要将 from 上的圆盘全部移动到 to 上，并且要保证小圆盘始终在大圆盘上。
 *
 * https://blog.csdn.net/i_silence/article/details/107133314
 * 这是一个经典的递归问题，分为三步求解：
 * 如果只有一个圆盘，那么只需要进行一次移动操作。
 *
 * 从上面的讨论可以知道，，显然 an = Math.pow(2, n) - 1，n 个圆盘需要移动 Math.pow(2, n) - 1 次。
 *
 * 示例代码如下：
 *
 * double result = Math.pow(2, 3); // 计算2的3次方
 *
 * System.out.println(result); // 输出8.0
 *
 * 列入：3个圆盘 即移动 7次
 *
 * @Author：Liuwq-Fuse
 * @Package：com.taozhugongboy.enumstatemachine.math.testDemo
 * @Project：fuse-tech-test-demo
 * @name：Hanoi
 * @Date：2023/8/28 13:55
 * @Filename：Hanoi
 */
public class Hanoi {
    /**
     * 移动
     *
     * @param n      n
     * @param from   从
     * @param buffer 缓冲
     * @param to     来
     * @author Liuwq
     * @date 2023/08/28
     */
    public static void move(int n, String from, String buffer, String to) {
        System.out.println("n " + n);
        if (n == 1) {
            System.out.println("from " + from + " to " + to);
            return;
        }
        move(n - 1, from, to, buffer);
        System.out.println("n1 " + n);
        move(1, from, buffer, to);
        System.out.println("n2 " + n);
        move(n - 1, buffer, from, to);
        System.out.println("n3 " + n);
    }

    public static void main(String[] args) {
        Hanoi.move(3, "H1", "H2", "H3");

        double out= Math.pow(2, 3) - 1;
        System.out.println("out " + out);
    }
}

