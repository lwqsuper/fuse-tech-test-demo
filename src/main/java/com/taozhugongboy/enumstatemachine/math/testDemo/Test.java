package com.taozhugongboy.enumstatemachine.math.testDemo;

import java.math.BigDecimal;

/**
 * @Author：Liuwq-Fuse
 * @Package：com.taozhugongboy.enumstatemachine.math.function
 * @Project：fuse-tech-test-demo
 * @name：Test
 * @Date：2023/8/25 16:24
 * @Filename：Test
 */
public class Test {
    /**
     * Java Math.exp() 方法返回自然数底数 e 的参数次方
     * @param args
     */
    public static void main1(String args[]){
//        double x = 11.635;
//        double y = 2.76;
//
//        System.out.printf("e 的值为 %.4f%n", Math.E);
//        System.out.printf("exp(%.3f) 为 %.3f%n", x, Math.exp(x));


//        BigDecimal a = new BigDecimal("2477.87610619469");
//        BigDecimal b = a.multiply(new BigDecimal(100));

//        BigDecimal a = new BigDecimal("24.7787610619469060");
//        BigDecimal b = a.multiply(new BigDecimal(99));
        BigDecimal a = new BigDecimal("80");
        BigDecimal b = a.divide(new BigDecimal(1.13),4,BigDecimal.ROUND_HALF_UP);
        System.out.printf(b.toString());

        BigDecimal nm = b.multiply(new BigDecimal(0.13));
        System.out.printf(">>>>>>>>>"+nm.toString());


//        BigDecimal a1 = new BigDecimal("80");
//        BigDecimal b1 = a1.multiply(new BigDecimal(0.13));
////        System.out.printf(b.toString());
//
//        BigDecimal b2 = a1.subtract(b1);
//
//
//
//        System.out.printf("b1:"+b1.toString());
//
//        System.out.printf("///b2:"+b2.setScale(2,BigDecimal.ROUND_HALF_UP).toString());
    }




        public static void main(String[] args) {
            long timestamp = System.currentTimeMillis();
            System.out.println("当前时间戳为：" + timestamp);
        }

}
