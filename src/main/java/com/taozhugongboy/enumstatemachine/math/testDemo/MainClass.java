package com.taozhugongboy.enumstatemachine.math.testDemo;

import java.util.stream.IntStream;

/**
 * @Author：Liuwq-Fuse
 * @Package：com.taozhugongboy.enumstatemachine.math.testDemo
 * @Project：fuse-tech-test-demo
 * @name：MainClass
 * @Date：2023/8/25 17:56
 * @Filename：MainClass
 */
public class MainClass {
    public static long fact(int x) {
        long p = 1;
        for (int i = 1; i <= x; i++)
            p = p * i;
        return p;
    }

    public static void main(String[] args) {
        double e = 1;
        for (int i = 1; i < 50; i++)
            e = e + 1 / (double) (fact(i));
        System.out.print("e = " + e);
    }

    /*public static double computeE(){
        return IntStream.iterate(0, k->k+1)
                .limit(100000)
                .mapToDouble(k->(3-4*k*k)/MathUtils.factorialDouble(2*k+1))
                .parallel()
                .sum();
    }*/
}
