package com.taozhugongboy.enumstatemachine.math.t;

import com.taozhugongboy.enumstatemachine.math.i.ActiveFunction;

/**
 * @Author：Liuwq-Fuse
 * @Package：com.taozhugongboy.enumstatemachine.math.t
 * @Project：fuse-tech-test-demo
 * @name：TMain
 * @Date：2023/8/25 17:40
 * @Filename：TMain
 */
public class TMain {
    protected ActiveFunction activeFunction;
    public double input(double x){
        double out = activeFunction.function(x);
        return out;
    }

}
