package com.taozhugongboy.enumstatemachine.async;

import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * @Author：Liuwq-Fuse
 * @Package：com.fusetech.system.test.async
 * @Project：fuse-tech
 * @name：AsyncServiceImpl
 * @Date：2023/7/18 17:59
 * @Filename：AsyncServiceImpl
 */
@Service
@Primary
public class AsyncServiceImpl implements AsyncService {

    @Override
    @Async("taskExecutor")
    public String sendSms(String callPrefix, String mobile, String actionType, String content) {
        String result = "发送成功";
        try {

            Thread.sleep(1000);
            //mesageHandler.sendSms(callPrefix, mobile, actionType, content);
            System.out.println("sendSms");
            //result =  "发送成功";
        } catch (Exception e) {
            //log.error("发送短信异常 -> ", e);
        }
        return result;
    }


    @Override
    @Async("taskExecutor")
    public String sendEmail(String email, String subject, String content) {
        String result = "发送成功";
        System.out.println(result);
        try {

            Thread.sleep(10000);
            //mesageHandler.sendsendEmail(email, subject, content);
            System.out.println("sendsendEmail");

        } catch (Exception e) {
            //log.error("发送email异常 -> ", e);
        }
        //result =  "发送成功";
        return result;
    }

}
