package com.taozhugongboy.enumstatemachine.async;

/**
 * @Author：Liuwq-Fuse
 * @Package：com.fusetech.system.test.async
 * @Project：fuse-tech
 * @name：AsyncService
 * @Date：2023/7/18 17:53
 * @Filename：AsyncService
 */
public interface AsyncService {
    String sendSms(String callPrefix, String mobile, String actionType, String content);

    String sendEmail(String email, String subject, String content);
}
