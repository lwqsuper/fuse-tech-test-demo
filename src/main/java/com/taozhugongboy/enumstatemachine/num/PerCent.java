package com.taozhugongboy.enumstatemachine.num;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;

/**
 * 百分数处理
 * @Author：Liuwq-Fuse
 * @name：PerCent
 * @Date：2023-11-13 10:55
 */
public class PerCent {
    public static void main(String[] args) throws ParseException {
//        Double num = (Double) NumberFormat.getInstance().parse("6%");
//        System.out.println(num);
        BigDecimal rate = new BigDecimal(13);
        Number parse = NumberFormat.getPercentInstance().parse(rate+"%");
        Double result = parse.doubleValue();
        System.out.println(new BigDecimal(result.toString()));
    }
}
